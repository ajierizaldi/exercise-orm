require('dotenv').config(); // Load .env file into process.env
const express = require('express'); // Express web server framework
const app = express(); // Create a new Express application
const bodyParser = require('body-parser'); // Parses the request body to be a readable json format
const cors = require('cors'); // Enable CORS for all requests
const port = process.env.PORT || 3030; // Set the port to listen on
const router = require('./router'); // Import the router
const YAML = require('yamljs'); // Parse YAML files
const swaggerUi = require('swagger-ui-express'); // Import swagger-ui-express
const apiDocs = YAML.load('./api-doc.yaml'); // Inisialisasi variable apiDocs dengan file api-doc.yaml

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json()); // Parses the request body to be a readable json format
app.use('/api', router); // Use the router
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(apiDocs)); // Use swagger-ui-express

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});