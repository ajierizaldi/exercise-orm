const express = require('express');
const router = express.Router();
const {list, create, update, destroy} = require('../controllers/categoryController');
const validate = require('../middleware/validate');
const {createCategoryRules} = require('../validators/rule');

router.get('/list', list);
router.post('/create', validate(createCategoryRules), create);
router.put('/update', update);
router.delete('/destroy', destroy);

module.exports = router;