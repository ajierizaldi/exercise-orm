const { body } = require('express-validator');
const createCategoryRules = [
    body('name').notEmpty().withMessage('Name is required'),
]

const registerRules = [
    body('email').isEmail().withMessage('email invalid').notEmpty().withMessage('Email is required'),
    body('username').notEmpty().withMessage('Username is required'),
    body('firstname').notEmpty().withMessage('Firstname is required'),
    body('lastname').notEmpty().withMessage('Lastname is required'),
    body('password').notEmpty().withMessage('Password is required')
]

module.exports = {
    createCategoryRules,
    registerRules
}