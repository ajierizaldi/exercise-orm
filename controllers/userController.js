const { user } = require('pg/lib/defaults')
const model = require('../models'),
    { genSalt, hash, compareSync } = require('bcrypt'),
    jwt = require('jsonwebtoken')

const cryptPassword = async (password) => {
    const salt = await genSalt(12)
    return hash(password, salt)
}

module.exports = {
    register: async (req, res) => {
        try {
            const data = await model.User.create({
                ...req.body,
                password: await cryptPassword(req.body.password)
            })
            return res.status(200).json({
                "message": 'User Successfully Registered',
                "error": 0,
                "data": data,
                "success": true
            })
        } catch (error) {
            return res.status(500).json({
                "message": 'User Registration Failed',
                "error": 1,
                "data": error,
                "success": false
            })
        }
    },
    login: async (req, res) => {
        try {
            const userExists = await model.User.findOne({
                where: {
                    email: req.body.email
                }
            })

            if (!userExists) {
                return res.status(200).json({
                    "message": 'User Not Found',
                    "error": 1,
                    "data": null,
                    "success": false
                })
            }

            if (!compareSync(req.body.password, userExists.password)) {
                const token = jwt.sign(
                    { id: userExists.id, username: userExists.username, email: userExists.email }, 'password!23',
                    { expiresIn: '1h' }
                )
            }

                return res.status(200).json({
                    "message": 'User Login Successful',
                    "error": 0,
                    "data": {
                        "token": token
                    }
                })
        } catch (error) {
            console.log(error)
            return res.status(500).json({
                "message": error,
                "error": error.code,
                "data": null,
                "success": false
            })
        }
    }
}
