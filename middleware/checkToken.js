const jwt = require('jsonwebtoken')

const checkToken = (req, res, next) => {
    let token = req.headers.authorization
    if (!token) {
        return res.status(401).json({
            "success": false,
            "error": 401,
            "message": 'No token provided.'
        })
    }

    // if provided with bearer then remove it
    if (token.toLowerCase().startsWith('bearer')) {
        token = token.slice('bearer', token.length).trim()
    }

    try {
        const jwtPayLoad = jwt.verify(token, 'password!23')

        if(!jwtPayLoad) {
            return res.status(401).json({
                "success": false,
                "error": 403,
                "message": 'Unauthorized',
                "data": null
            })
        }

        res.locals.user = jwtPayLoad
        next()

    } catch (error) {
        return res.status(401).json({
            "success": false,
            "error": 401,
            "message": 'Invalid token.'
        })
    }
}

module.exports = checkToken